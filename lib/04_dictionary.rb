class Dictionary
  attr_accessor :entries

  def initialize
    @entries = Hash.new(0)
  end

  def add(input)
    if input.is_a?(Hash)
      @entries[input.keys[0]] = input.values[0]
    else
      input.split.each { |word| @entries[word] = nil }
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(word)
    @entries.keys.any? { |key| key == word }
  end

  def find(string)
    @entries.select { |k, _v| k.include?(string) }
  end

  def printable
    keywords.map do |k|
      %Q{[#{k}] "#{@entries[k]}"}
    end.join("\n")
  end

end
