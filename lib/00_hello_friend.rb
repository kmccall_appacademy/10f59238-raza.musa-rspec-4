class Friend
  def greeting(someone = nil)
    if someone
      "Hello, #{someone}!"
    else
      "Hello!"
    end
  end
end
