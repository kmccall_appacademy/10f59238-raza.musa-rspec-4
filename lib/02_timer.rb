class Timer

attr_accessor :time, :seconds

def initialize
  @time = 0
  @seconds = 0
end



def time_string
  hour_time = padded(seconds / 3600)
  min_time = padded((seconds / 60) % 60)
  sec_time = padded(seconds % 60)
  "#{hour_time}:#{min_time}:#{sec_time}"
end

def padded(n)
  if n < 10
    "0#{n}"
  else
    "#{n}"
  end
end

end
