class Book

  attr_accessor :title

  def title=(title)
    not_capital = ["and", "in", "the", "of", "a", "an"]
    new_words = title.split.map.with_index do |word, idx|
      if idx == 0 && not_capital.include?(word)
        word.capitalize
      elsif not_capital.include?(word)
        word
      else
        word.capitalize
      end
    end
    @title = new_words.join(" ")
  end
end
