class Temperature
  attr_accessor :hash, :fahrenheit, :celsius

  def initialize(hash)
    @hash = hash
    @fahrenheit = hash[:f]
    @celsius = hash[:c]
  end

  def in_fahrenheit
    return @fahrenheit if hash.keys.include?(:f)
    (@celsius * 9 / 5.to_f) + 32
  end

  def in_celsius
    return @celsius if hash.keys.include?(:c)
    (@fahrenheit - 32) * 5 / 9
  end


  def self.from_celsius(input)
    Temperature.new(:c => input)
  end

  def self.from_fahrenheit(input)
    Temperature.new(:f => input)
  end

end

class Celsius < Temperature
  def initialize(from_celsius)
    @Temp = from_celsius
  end

end
